# Sonatype Central Portal Publisher

[![Gradle Plugin Portal](https://img.shields.io/gradle-plugin-portal/v/net.thebugmc.gradle.sonatype-central-portal-publisher?logo=gradle&label=Gradle%20Plugin%20Portal)](https://plugins.gradle.org/plugin/net.thebugmc.gradle.sonatype-central-portal-publisher)
[![MIT License](https://img.shields.io/badge/license-MIT-blue)](LICENSE-MIT)
[![Java Version](https://img.shields.io/endpoint?url=https%3A%2F%2Fgitlab.com%2Fthebugmc%2Fsonatype-central-portal-publisher%2F-%2Fraw%2Fmaster%2Fjava-version-badge.json)](https://gitlab.com/thebugmc/sonatype-central-portal-publisher/-/blob/master/build.gradle.kts#L5)

This Gradle plugin provides a way to publish to [Sonatype's Central Repository](https://central.sonatype.com/)
via the Central Portal.

> By the time of writing this Gradle plugin, the way to publish was via OSSRH, the Central Portal
> was still in the Early Access (thus often called "Publisher Early Access").

> Since `1.2.0` the plugin supports Android Libraries (`.aar`, `com.android.library`).

> Note that this plugin is not officially made/supported by Sonatype.

## Setup

> Examples are written in the `build.gradle.kts` format in sections below.

### Plugin

Add the plugin to your project from the [Gradle Plugin Portal](https://plugins.gradle.org/plugin/net.thebugmc.gradle.sonatype-central-portal-publisher):

```kts
// build.gradle.kts
plugin {
    id("net.thebugmc.gradle.sonatype-central-portal-publisher") version "..."
}
```

> Note the [Java version](https://gitlab.com/thebugmc/sonatype-central-portal-publisher/-/blob/master/build.gradle.kts#L8)
> of this plugin (also mentioned as a badge at the top of this file) - you should be using at least
> that version to run your Gradle instance.

While installing the plugin, ensure your project is set up as usual for Gradle libraries for
publishing. This might be already the case for a regular Gradle Java library. In case you are making
an Android library should also have something like this example:

```kts
// build.gradle.kts
description = "Android Library test."
group = "com.example.testlib"
version = "0.1.0"
```

### Signing

First of all, [generate the necessary key pairs and distribute them](https://central.sonatype.org/publish/requirements/gpg/).

After this, to set up the signing step you will need one of the following configurations as
described in the [Gradle documentation](https://docs.gradle.org/current/userguide/signing_plugin.html).
If you have some more complicated options to put in, check that page. If your setup is simple, the
following examples should be enough:

```properties
# gradle.properties
signing.keyId=...
signing.password=...
signing.secretKeyRingFile=...
```

With GnuPG the setup looks like this:

```kts
// build.gradle.kts
signing {
    useGpgCmd()
}
```

```properties
# gradle.properties
signing.gnupg.executable=...
signing.gnupg.keyName=...
signing.gnupg.passphrase=...
```

### Authentication

After signing, you can add your Sonatype Central Portal **user token** for auth (similarly to how it
is done in the [official maven plugin documentation](https://central.sonatype.org/publish/publish-portal-maven/#credentials))
with one of these two ways:

1. Using `gradle.properties`:

    ```properties
    # gradle.properties
    centralPortal.username=...user token...
    centralPortal.password=...token passphrase...
    ```

2. Using `centralPortal` level in your project's `build.gradle.kts`:

    ```kts
    // build.gradle.kts
    centralPortal {
        username = "...user token..."
        password = "...token passphrase..."
    }
    ```

### Naming

You may perform the setup in any `build.gradle(.kts)` file (i.e. subprojects). The name used
everywhere (`artifactId`, bundle name, etc) by default is `rootProject.name`. This means on Android
you will have a `my-project` name instead of `app` (or the name you have set up).

This behavior can be overridden with the following option:

```kts
// build.gradle.kts
centralPortal {
    name = "my-subproject-custom-name"
}
```

All files related to Android `release` will be renamed with the `project.version`. You do not need
to manually set up your project in a way that does the replacement (e.g. no `archivesBaseName`).

### POM

The primary thing left to make it work is to [set up your project's POM](https://docs.gradle.org/current/userguide/publishing_maven.html#sec:modifying_the_generated_pom)
so that the project will pass validation after publishing:

```kts
// build.gradle.kts
centralPortal {
    pom {
        // `name = centralPortal.name/rootProject.name`, `description = project.description` and
        // `packaging = "jar/aar"` are applied automatically, but you can override them here
        url = "..."
        licenses {
            license {
                name = "..."
                url = "https://opensource.org/licenses/..."
            }
        }
        developers {
            developer {
                name = "..."
                email = "..."
                organization = "..."
                organizationUrl = "..."
            }
        }
        scm {
            connection = "scm:..."
            developerConnection = "scm:..."
            url = "..."
        }
        /*...*/
    }
}
```

The `packaging` is set to either `jar` or `aar` depending on if you have Android Library plugin
(`com.android.library`) applied or not. As mentioned above, if you want to specify a different
`packaging` value, you can do that yourself in the `pom` block.

### Version Mapping

The plugin also supports [customizing dependencies versions](https://docs.gradle.org/current/userguide/publishing_maven.html#publishing_maven:resolved_dependencies):

```kts
// build.gradle.kts
centralPortal {
    versionMapping {
        /*...*/
    }
}
```

### Kotlin Javadoc

If you try to make a publication (or a bundle) a `...-javadoc.jar` will be generated. However, if
you use Kotlin the default `:javadoc` task will be unable to process `.kt` files and greet you with
the following message:

```text
Detected `.kt` file that will be ignored by the `:javadoc` task. Install `org.jetbrains.dokka` Gradle plugin to generate JavaDocs for `.kt` files.
```

Following that advice, this plugin will adapt and use the `:dokkaJavadoc` task instead, with its
module set to `centralPortal.name`/`rootProject.name`.

### Specifying tasks and Closed Source

The Central Portal requires you to publish your artifacts with JavaDocs and sources. However, the
only requirement is that the `.jar` files are included in the bundle, and thus making them empty is
an [actually viable option for closed source](https://central.sonatype.org/faq/closed-source/#provide-files-checksums).
For this, the plugin supports specifying custom `sourcesJarTask` and `javadocJarTask`.

```kts
// build.gradle.kts
centralPortal {
    sourcesJarTask = tasks.create<Jar>("sourcesEmptyJar") {
        archiveClassifier = "sources"
    }
    javadocJarTask = tasks.create<Jar>("javadocEmptyJar") {
        archiveClassifier = "javadoc"
    }
}
```

Note the `archiveClassifier` options: these are names added at the end of the included `.jar` files,
for example `my-project-sources.jar`. Without it the `.jar`s may collide silently or be rejected by
the Central API; to make sure that never happens - make sure your generated bundle from
`:checksumBundle` matches the [Central Portal documentation](https://central.sonatype.org/publish/publish-portal-upload/).

Additionally, the `jarTask` is also customizable:

```kts
// build.gradle.kts
centralPortal {
    jarTask = tasks.jar
}
```

### Publishing

Central Portal allows you to publish the uploads in the panel manually. For this, you have to
specify the `USER_MANAGED` `publishingType`:

```kts
// build.gradle.kts
import net.thebugmc.gradle.sonatypepublisher.PublishingType.USER_MANAGED // at the top of the file

centralPortal {
    publishingType = USER_MANAGED
}
```

The default type is `AUTOMATIC`, which publishes immediately after upload.

### Example with all features

```kts
// build.gradle.kts
import net.thebugmc.gradle.sonatypepublisher.PublishingType.USER_MANAGED

plugins {
    // id("org.jetbrains.kotlin.android")
    id("java") /* OR */ id("com.android.library")
    // id("org.jetbrains.dokka")
    id("net.thebugmc.gradle.sonatype-central-portal-publisher") version "..."
}

description = "Example Java OR Android Library"
group = "com.example"
version = "0.1.0"

signing {
    useGpgCmd()
}

centralPortal { // all settings here are optional, but you better setup `pom`
    username = "...user token..."
    password = "...token passphrase..."
    
    publishingType = USER_MANAGED

    name = rootProject.name

    jarTask = /*...*/
    sourcesJarTask = /*...*/
    javadocJarTask = /*...*/

    pom {
        url = "..."
        licenses {
            license {
                name = "..."
                url = "https://opensource.org/licenses/..."
            }
        }
        developers {
            developer {
                name = "..."
                email = "..."
                organization = "..."
                organizationUrl = "..."
            }
        }
        scm {
            connection = "scm:..."
            developerConnection = "scm:..."
            url = "..."
        }
        /*...*/
    }
    versionMapping { /*...*/ }
}
```

## Usage

Upon reloading Gradle with the plugin active you will notice that the main `:publishToCentralPortal`
task is available under the `central portal publishing` group. Here is the task chain:

- `:jar`/`:bundleReleaseAar` - (not from this plugin) will create the basic artifact to publish;
- `:sourcesJar` and `:javadocJar` - will create such artifacts (can be overridden);
- signing happens next for the Maven publication;
- `:generateBundle` - will bundle all of them into a `...-bundle.zip` file under `build/bundles`;
- `:checksumBundle` - will checksum all files into a `...-bundle-checksum.zip` (MD5, SHA-1);
- `:publishToCentralPortal` - will publish the artifact to the Central Portal via the Central API.

Your primary use-case task is `:publishToCentralPortal`, but you may also try using
`:checksumBundle` to see the bundle that will be uploaded to the Central Portal (for example, for
closed source - to confirm that `...-sources.jar` is empty).
