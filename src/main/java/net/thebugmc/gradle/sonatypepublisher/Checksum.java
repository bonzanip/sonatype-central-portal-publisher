package net.thebugmc.gradle.sonatypepublisher;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static java.lang.Math.min;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Task that calculates checksums for every non-{@code .asc} file in an archive.
 *
 * @see #calculateFor
 */
public class Checksum extends DefaultTask {
    private AbstractArchiveTask archive;

    /**
     * Specifies what file to calculate checksums for.
     */
    public void calculateFor(AbstractArchiveTask archive) throws IllegalStateException {
        if (this.archive != null && this.archive != archive)
            throw new IllegalStateException("Already calculating checksum for " + this.archive);

        dependsOn(archive);
        this.archive = archive;
    }

    public String toString() {
        return "Checksum for " + archive;
    }

    private static final int UPPER_DIGIT = 0xF0;
    private static final int LOWER_DIGIT = 0x0F;
    private static final int HEX_RADIX = 16;

    private static String hex(byte[] bytes) {
        var result = new StringBuilder();
        for (var b : bytes)
            result
                .append(Character.forDigit((b & UPPER_DIGIT) >> 4, HEX_RADIX))
                .append(Character.forDigit(b & LOWER_DIGIT, HEX_RADIX));
        return result + "";
    }

    /**
     * File in which the checksum output will be present.
     */
    public File outputFile() {
        var archiveFile = archive.getArchiveFile().get().getAsFile();
        var ext = "." + archive.getArchiveExtension().get();
        var name = archiveFile
            .getName()
            .substring(0, archiveFile.getName().length() - ext.length());
        return new File(archiveFile.getParentFile(), name + "-checksum" + ext);
    }

    /**
     * Copy over files and add their checksums to the {@link #outputFile() output file}.
     *
     * @throws IOException              If any IO exception occurs.
     * @throws NoSuchAlgorithmException If unable to find MD5 or SHA-1 algorithms.
     */
    @TaskAction
    public void writeChecksums() throws IOException, NoSuchAlgorithmException {
        var archiveFile = archive.getArchiveFile().get().getAsFile();
        try (
            var input = new ZipInputStream(new FileInputStream(archiveFile));
            var output = new ZipOutputStream(new FileOutputStream(outputFile()))
        ) {
            ZipEntry entry;
            while ((entry = input.getNextEntry()) != null) {
                if (entry.isDirectory()) continue;

                final var buffer = new byte[1024];

                var originalName = entry.getName();
                if (originalName.contains(".asc")) {
                    try {
                        output.putNextEntry(new ZipEntry(originalName));

                        var available = entry.getSize();
                        while (available > 0) {
                            var read = input.read(buffer, 0, (int) min(buffer.length, available));
                            output.write(buffer, 0, read);
                            available -= read;
                        }
                    } finally {
                        output.closeEntry();
                    }
                    continue;
                }
                var md5Name = originalName + ".md5";
                var sha1Name = originalName + ".sha1";

                var md5 = MessageDigest.getInstance("MD5");
                var sha1 = MessageDigest.getInstance("SHA-1");

                try {
                    output.putNextEntry(new ZipEntry(originalName));

                    var available = entry.getSize();
                    while (available > 0) {
                        var read = input.read(buffer, 0, (int) min(buffer.length, available));
                        md5.update(buffer, 0, read);
                        sha1.update(buffer, 0, read);
                        output.write(buffer, 0, read);
                        available -= read;
                    }
                } finally {
                    output.closeEntry();
                }

                try {
                    output.putNextEntry(new ZipEntry(md5Name));
                    output.write(hex(md5.digest()).getBytes(UTF_8));
                } finally {
                    output.closeEntry();
                }

                try {
                    output.putNextEntry(new ZipEntry(sha1Name));
                    output.write(hex(sha1.digest()).getBytes(UTF_8));
                } finally {
                    output.closeEntry();
                }
            }
        }
    }
}
