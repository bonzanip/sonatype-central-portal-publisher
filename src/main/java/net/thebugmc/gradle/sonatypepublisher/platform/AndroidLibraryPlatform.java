package net.thebugmc.gradle.sonatypepublisher.platform;

import com.android.build.gradle.LibraryExtension;
import com.android.build.gradle.internal.api.DefaultAndroidSourceDirectorySet;
import com.android.build.gradle.tasks.BundleAar;
import kotlin.Unit;
import org.gradle.api.InvalidUserDataException;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.UnknownDomainObjectException;
import org.gradle.api.component.SoftwareComponent;
import org.gradle.api.file.FileTree;
import org.gradle.api.logging.Logger;
import org.gradle.api.provider.Provider;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.javadoc.Javadoc;
import org.jetbrains.dokka.gradle.DokkaTask;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.gradle.api.tasks.SourceSet.MAIN_SOURCE_SET_NAME;

/**
 * {@code com.android.library} platform that assumes Android plugin classes are available.
 */
public final class AndroidLibraryPlatform implements Platform {
    /**
     * The name of the generated component.
     */
    public static final String COMPONENT_NAME = "default";

    /**
     * Sets up the platform. Will set {@code android} library extension publishing to all variants.
     */
    public AndroidLibraryPlatform(Logger log, Project target) {
        log.info("Setting up Android publishing");
        var android = target.getExtensions()
            .getByType(LibraryExtension.class);

        var publishing = android.getPublishing();
        publishing.multipleVariants(COMPONENT_NAME, a -> {
            // Using `a.allVariants()` includes `<optional>true</optional>` in the POM for all
            // dependencies. Let's specify "release" build type only, since it's the only one used
            // in this platform's code anyway (fixed in 1.2.1).
            a.includeBuildTypeValues("release");
            return Unit.INSTANCE;
        });
    }

    public FileTree mainSourceSetSources(Logger log, Project target) {
        log.info("Detecting the `main` Android sources");
        var mainSourceSet = target
            .getExtensions()
            .getByType(LibraryExtension.class)
            .getSourceSets()
            .getByName(MAIN_SOURCE_SET_NAME);
        var java = mainSourceSet.getJava().getSourceFiles();
        var kotlin
            = ((DefaultAndroidSourceDirectorySet) mainSourceSet.getKotlin()).getSourceFiles();
        return java.plus(kotlin);
    }

    public BundleAar jarTask(Logger log, TaskContainer tasks) {
        log.info("Detecting the `:bundleReleaseAar` task");
        var aar = (BundleAar) tasks.getByName("bundleReleaseAar");
        var version = aar.getArchiveVersion();
        if (!version.isPresent())
            throw new InvalidUserDataException(
                "Your Android project is not setup for publishing. "
                + "Please follow the plugin installation guide: "
                + "https://gitlab.com/thebugmc/sonatype-central-portal-publisher#plugin"
            );
        aar.getArchiveFileName()
            .set(
                aar.getArchiveFileName()
                    .get()
                    .replaceAll(
                        "(.*)-release.aar",
                        "$1-%s.aar".formatted(version.getOrElse("unspecified"))
                    )
            );
        return aar;
    }

    public Task javadocTask(
        Logger log,
        Project target,
        TaskContainer tasks,
        FileTree mainSourceSetSources,
        Provider<String> nameProvider
    ) {
        if (target.getPlugins().hasPlugin("org.jetbrains.dokka")) {
            log.info("Detecting Dokka `:dokkaJavadoc` task (via `org.jetbrains.dokka`)");
            var task = (DokkaTask) tasks.getByName("dokkaJavadoc");
            task.getModuleName().set(nameProvider);
            return task;
        }

        log.info("Generating the Android `:javadoc` Java-only task");
        var task = tasks.create("javadoc", Javadoc.class);
        var detectedKt = new AtomicBoolean();
        task.setSource(mainSourceSetSources.filter(file -> {
            if (file.getName().endsWith(".kt") && !detectedKt.get()) {
                detectedKt.set(true);
                log.warn(
                    "Detected `.kt` file that will be ignored by the `:javadoc` task. Install "
                    + "`org.jetbrains.dokka` Gradle plugin to generate JavaDocs for `.kt` files."
                );
            }

            return file.getName().endsWith(".java");
        }));
        return task;
    }

    public String defaultPackaging() {
        return "aar";
    }

    public SoftwareComponent component(
        Logger log,
        Project target
    ) throws UnknownDomainObjectException {
        return target.getComponents().getByName(COMPONENT_NAME);
    }
}
